#include <stdio.h>

#include "CuTest.h"

CuSuite* GetTestSuite(void);

int RunAllTests(void)
{
	CuString *output = CuStringNew();
	CuSuite* suite = CuSuiteNew();

	CuSuiteAddSuite(suite, GetTestSuite());

	CuSuiteRun(suite);
	CuSuiteSummary(suite, output);
	CuSuiteDetails(suite, output);
	printf("%s\n", output->buffer);
   
   return (suite->failCount > 0) ? 1 : 0;
}

int main(int argc, char * args[])
{
   int result = RunAllTests();
   FILE *fh;
   
   if(result == 0)
   {
      fh = fopen("testout.temp", "w");
      fclose(fh);
   }
   
   return result;
}
