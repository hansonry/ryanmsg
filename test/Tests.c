#include "CuTest.h"
#include "ryanmsg.h"
#include <stdlib.h>

static int ConvertStringToByteArray(unsigned char * buffer, const char * str)
{
   char * nextstr;
   int size = 0;
   
   (*buffer) = (unsigned char)strtol(str, &nextstr, 0);
   while(str != nextstr)
   {
      buffer ++;
      size ++;
      str = nextstr;
      (*buffer) = (unsigned char)strtol(str, &nextstr, 0);
   }
   return size;
}

#define CuAssertByteStringEquals(tc, ex, ac, acs) \
do { \
   unsigned char expected_buffer[256]; \
   int expected_size;                  \
   expected_size = ConvertStringToByteArray(expected_buffer, (ex)); \
   CuAssertBufferEquals_LineMsg((tc), __FILE__, __LINE__, NULL, expected_buffer, expected_size, (const unsigned char*)(ac), (acs)); \
} while(0)


static void TestCuStringAppendFormat(CuTest* tc)
{
   struct ryanmsgset * set;
   struct ryanmsg * msg;
   const unsigned char * buffer;
   //CuFail(tc, "Fail yes");

   set = ryanmsgset_create();

   ryanmsgset_startmessage(set, "signal1");
   ryanmsgset_endmessage(set);

   ryanmsgset_startmessage(set, "signal2");
   ryanmsgset_endmessage(set);

   msg = ryanmsg_create(set, "signal1");

   buffer = ryanmsg_buffer(msg);

   CuAssertByteStringEquals(tc, "0x00 0x01", buffer, 2);
   
   ryanmsg_destroy(msg);

   msg = ryanmsg_create(set, "signal2");

   buffer = ryanmsg_buffer(msg);

   //CuAssertBufferInlineEquals(tc, {0x02},  buffer, 1); 

   ryanmsg_destroy(msg);

   ryanmsgset_destroy(set);
   
}

static void TestMessageSet(CuTest* tc)
{
   struct ryanmsgset * set;

   set = ryanmsgset_create();

   ryanmsgset_startmessage(set, "message0");
   ryanmsgset_endmessage(set);

   ryanmsgset_startmessage(set, "message1");
   ryanmsgset_endmessage(set);

   CuAssertIntEquals(tc, -1, ryanmsgset_getmessageindex(set, "not_a_message"));
   CuAssertIntEquals(tc,  0, ryanmsgset_getmessageindex(set, "message0"));
   CuAssertIntEquals(tc,  1, ryanmsgset_getmessageindex(set, "message1"));

   ryanmsgset_destroy(set);
}

static void TestDepthOneMessage(CuTest * tc)
{
   struct ryanmsgset * set;
   struct ryanmsg * msg;
   const unsigned char * buffer;
   unsigned char expected_buffer[256];
   int expected_size;
   int size;

   set = ryanmsgset_create();

   ryanmsgset_startmessage(set, "message_empty");
   ryanmsgset_endmessage(set);


   ryanmsgset_startmessage(set, "message_with_a1");
   ryanmsgset_addint8(set, "a");
   ryanmsgset_endmessage(set);

   ryanmsgset_startmessage(set, "message_with_a2");
   ryanmsgset_addint8(set, "a");
   ryanmsgset_endmessage(set);

   ryanmsgset_addint8(set, "b");
   

   CuAssertIntEquals(tc, -1, ryanmsgset_messagesize(set, "not_a_message"));
   CuAssertIntEquals(tc,  2, ryanmsgset_messagesize(set, "message_empty"));   
   CuAssertIntEquals(tc,  3, ryanmsgset_messagesize(set, "message_with_a1"));
   CuAssertIntEquals(tc,  3, ryanmsgset_messagesize(set, "message_with_a2"));

   msg = ryanmsg_create(set, "message_with_a1");

   buffer = ryanmsg_buffer(msg);
   size = ryanmsg_size(msg);
   CuAssertByteStringEquals(tc, " 0 1 0xFF",  buffer, size);
   
   
   ryanmsg_destroy(msg);

   ryanmsgset_destroy(set);
}

CuSuite* GetTestSuite(void)
{
	CuSuite* suite = CuSuiteNew();

	SUITE_ADD_TEST(suite, TestMessageSet);
	SUITE_ADD_TEST(suite, TestDepthOneMessage);
	SUITE_ADD_TEST(suite, TestCuStringAppendFormat);

	return suite;
}

