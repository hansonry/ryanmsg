#ifndef __RYANMSG_H__
#define __RYANMSG_H__
#include <stddef.h>

struct ryanmsgset;

struct ryanmsgset * ryanmsgset_create(void);
void ryanmsgset_destroy(struct ryanmsgset * set);


void ryanmsgset_startmessage(struct ryanmsgset * set, const char * message_name);
void ryanmsgset_endmessage(struct ryanmsgset * set);

int ryanmsgset_getmessageindex(struct ryanmsgset * set, const char * message_name);
void ryanmsgset_addint8(struct ryanmsgset * set, const char * name);

int ryanmsgset_messagesize(struct ryanmsgset * set, const char * name);

struct ryanmsg;

struct ryanmsg * ryanmsg_create(struct ryanmsgset * set, const char * message_name);
void ryanmsg_destroy(struct ryanmsg * msg);

const unsigned char * ryanmsg_buffer(struct ryanmsg * msg);

int ryanmsg_size(struct ryanmsg * msg);

#endif // __RYANMSG_H__

