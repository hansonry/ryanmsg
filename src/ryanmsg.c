#include <string.h>
#include <stdlib.h>
#include "ryanmsg.h"

#define DEFAULT_GROWBY 32

// Ryan Message Set
struct rms_msg
{
   char * name;
   int sizeInBytes;
};

struct ryanmsgset
{
   struct rms_msg * msg_base;
   int msg_size;
   int msg_count;
   int currentMessageIndex;
};

struct ryanmsgset * ryanmsgset_create(void)
{
   struct ryanmsgset * set;
   set = malloc(sizeof(struct ryanmsgset));

   set->msg_base  = malloc(sizeof(struct rms_msg) * DEFAULT_GROWBY);
   set->msg_size  = DEFAULT_GROWBY;
   set->msg_count = 0;
   set->currentMessageIndex = -1;

   return set;
}

void ryanmsgset_destroy(struct ryanmsgset * set)
{
   int i;
   for(i = 0; i < set->msg_count; i++)
   {
      free(set->msg_base[i].name);
   }
   free(set->msg_base);
   free(set);
}



void ryanmsgset_startmessage(struct ryanmsgset * set, const char * message_name)
{
   if(set->msg_count >= set->msg_size)
   {
      set->msg_size = set->msg_count + DEFAULT_GROWBY;
      set->msg_base = realloc(set->msg_base, sizeof(struct rms_msg) * set->msg_size);
   }
   set->msg_base[set->msg_count].name = strdup(message_name);
   set->msg_base[set->msg_count].sizeInBytes = 2;
   set->currentMessageIndex = set->msg_count;
   set->msg_count ++;
}

void ryanmsgset_endmessage(struct ryanmsgset * set)
{
   set->currentMessageIndex = -1;
}

int ryanmsgset_getmessageindex(struct ryanmsgset * set, const char * message_name)
{
   int i, index;
   index = -1;
   for(i = 0; i < set->msg_count; i++)
   {
      if(strcmp(set->msg_base[i].name, message_name) == 0)
      {
         index = i;
         break;
      }
   }
   return index;
}

void ryanmsgset_addint8(struct ryanmsgset * set, const char * name)
{
   if(set->currentMessageIndex == -1)
   {
      return;
   }
   set->msg_base[set->currentMessageIndex].sizeInBytes += 1;
}

int ryanmsgset_messagesize(struct ryanmsgset * set, const char * name)
{
   int index;
   index = ryanmsgset_getmessageindex(set, name);
   if(index == -1)
   {
      return -1;
   }
   
   return set->msg_base[index].sizeInBytes;
}

// Ryan Message

struct ryanmsg
{
   unsigned char * buffer;
};

struct ryanmsg * ryanmsg_create(struct ryanmsgset * set, const char * message_name)
{
   struct ryanmsg * msg;
   msg = malloc(sizeof(struct ryanmsg));
   msg->buffer = malloc(10);
   return msg;
}

void ryanmsg_destroy(struct ryanmsg * msg)
{
   free(msg->buffer);
   free(msg);
}


const unsigned char * ryanmsg_buffer(struct ryanmsg * msg)
{
   msg->buffer[0] = 0;
   msg->buffer[1] = 1;
   msg->buffer[2] = 0xFF;
   return msg->buffer;
}

int ryanmsg_size(struct ryanmsg * msg)
{
   return 3;
}

