settings = NewSettings()

exe_prefix = "./"
if family == "windows" then
   settings.cc.defines:Add("_CRT_SECURE_NO_WARNINGS")
   settings.cc.defines:Add("strdup=_strdup")
   exe_prefix = ""
end

settings.cc.includes:Add("src")
settings.cc.includes:Add("test")
settings.cc.includes:Add("test/cutest-1.5")

source = Collect("src/*.c", "test/*.c", "test/cutest-1.5/CuTest.c")
objects = Compile(settings, source);
exe = Link(settings, "ryanmsgtest", objects)

AddJob("testout.temp", "Running Tests", exe_prefix .. "ryanmsgtest", exe)

